+++
title = "ブログを開設しました"
date = 2020-04-02T15:01:46+09:00
draft = false
author = "Nakaya"
authorTwitter = "eniehack" #do not include @
cover = ""
tags = ["infomation"]
keywords = ["", ""]
description = "Hugoを用いてブログ及びWebサイトを作りました。"
showFullContent = false
+++

# ブログを開設しました

こんにちは。群馬高専電算部です。突然ですがブログを開設させていただきました。

## このブログについて
このブログでは部員の大会での戦績や、部員の制作物紹介、Twitterには長文のため書けないことをメインに書いていくつもりです。Webサイトを管理できる人が少ないので続くかはわかりませんが、暖かく見守っていただけると幸いです。
これからよろしくお願いします。
